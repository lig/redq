use anyhow::Result;
use clap::Parser;
use futures::future;
use log::info;

#[derive(Parser, Debug)]
struct Args {
    /// Possible values are: `off`, `error`, `warn`, `info`, `debug`, `trace`
    #[clap(long, default_value = "info")]
    log_level: simplelog::LevelFilter,
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();

    simplelog::TermLogger::init(
        args.log_level,
        simplelog::Config::default(),
        simplelog::TerminalMode::Mixed,
        simplelog::ColorChoice::Auto,
    )?;

    future::try_join_all(vec![tokio::spawn(redq::server())]).await?;

    info!("Exiting");
    Ok(())
}
