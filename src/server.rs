use std::time::Duration;

use anyhow::*;
use tokio::{io::AsyncReadExt, net, time};

const LISTEN_ADDR: &str = "0.0.0.0:4150";
const TIMEOUT: u64 = 30;

pub async fn run() -> Result<()> {
    info!("Starting server");

    let listener = net::TcpListener::bind(LISTEN_ADDR).await?;

    loop {
        let (socket, addr) = listener.accept().await?;
        info!("Got connection from `{}`", addr.ip());

        tokio::spawn(async {
            if let Err(e) = handle_client(socket).await {
                error!("{}", e);
            }
        });
    }
}

async fn handle_client(socket: net::TcpStream) -> Result<()> {
    handle_protocol_magic(socket).await?;

    Ok(())
}

async fn handle_protocol_magic(mut socket: net::TcpStream) -> Result<(), Error> {
    debug!("Waiting for the protocol magic");

    let mut magic_buf = vec![0; 8];
    time::timeout(
        Duration::from_secs(TIMEOUT),
        socket.read_exact(&mut magic_buf),
    )
    .await
    .context("timeout receiving protocol magic")?
    .context("error receiving protocol magic")?;

    if String::from_utf8_lossy(&magic_buf) != "  RED V0" {
        bail!("Unknown protocol magic");
    }

    debug!("Accepted protocol magic");

    Ok(())
}
