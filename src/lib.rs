#[macro_use]
extern crate log;
extern crate simplelog;

mod server;

pub use server::run as server;
